package net.nix.mod;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.SideOnly;
import flaxbeard.steamcraft.api.IWrenchDisplay;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;

/**
 * Created by Егорка on 28.10.2014.
 */
public class SteamcraftIntegrationHandler {

    @SideOnly(value=cpw.mods.fml.relauncher.Side.CLIENT)
    @SubscribeEvent
    public void onDrawScreen(net.minecraftforge.client.event.RenderGameOverlayEvent.Post event)
    {
        Minecraft mc = Minecraft.getMinecraft();
        MovingObjectPosition pos = mc.objectMouseOver;
        TileEntity aTileEntity = Minecraft.getMinecraft().theWorld.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);
        if(event.type == net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType.ALL)
        {
            if (pos != null && mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof Screwdriver) {
                if(aTileEntity instanceof IWrenchDisplay)
                {
                    ((IWrenchDisplay)aTileEntity).displayWrench(event);
                }
            }
        }
    }
}
