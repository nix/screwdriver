package net.nix.mod;

import java.util.Arrays;

import Reika.RotaryCraft.API.Screwdriverable;
import Reika.RotaryCraft.API.ShaftMachine;
import appeng.api.implementations.items.IAEWrench;
import buildcraft.api.tools.IToolWrench;
import carpentersblocks.api.ICarpentersHammer;
import cofh.api.block.IDismantleable;
import cofh.api.item.IToolHammer;
import cofh.lib.util.helpers.ServerHelper;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import crazypants.enderio.item.IYetaWrench;
import flaxbeard.steamcraft.api.IWrenchable;
import mekanism.api.IMekWrench;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.oredict.OreDictionary;
import powercrystals.minefactoryreloaded.api.IToolHammerAdvanced;

public class Screwdriver extends Item implements IToolWrench, IAEWrench, IToolCrowbar, ICarpentersHammer, mrtjp.projectred.api.IScrewdriver, IToolHammer, IToolHammerAdvanced, IYetaWrench, IMekWrench
{

    public Screwdriver()
    {
        super();
        maxStackSize = 1;
        setCreativeTab(CreativeTabs.tabTools);
    }

    @Override
    public boolean doesSneakBypassUse(World world, int x, int y, int z, EntityPlayer player) {
        return true;
    }

    public boolean onItemUseFirst(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int X, int Y, int Z, int par7, float par8, float par9, float par10)
    {
        par2EntityPlayer.swingItem();
        if (par3World.isRemote) {
            return false;
        }
        Block TargetBlock = par3World.getBlock(X, Y, Z);
        if (TargetBlock == null) return false;
        byte Meta = (byte)par3World.getBlockMetadata(X, Y, Z);
        final ItemStack is = new ItemStack(TargetBlock, 1, Meta);
        ForgeDirection side = ForgeDirection.getOrientation(par7);
        TileEntity aTileEntity = par3World.getTileEntity(X, Y, Z);

        par3World.playSoundEffect((double) ((float) X + 0.5F), (double) ((float) Y + 0.5F), (double) ((float) Z + 0.5F), TargetBlock.stepSound.getStepResourcePath(), (TargetBlock.stepSound.getVolume() + 1.0F) / 2.0F, TargetBlock.stepSound.getPitch() * 0.8F);

        if (!par2EntityPlayer.isSneaking()) {
            if (OreDictionary.getOreID(is) == OreDictionary.getOreID("logWood") || TargetBlock == Blocks.hay_block) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, (Meta + 4) % 12, 3);
                return true;
            }
            if (TargetBlock == Blocks.piston || TargetBlock == Blocks.sticky_piston || TargetBlock == Blocks.dispenser || TargetBlock == Blocks.dropper) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, (Meta + 1) % 6, 3);
                return true;
            }
            if (TargetBlock == Blocks.unpowered_repeater || TargetBlock == Blocks.powered_repeater) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, (Meta / 4) * 4 + (((Meta % 4) + 1) % 4), 3);
                return true;
            }
            if (TargetBlock == Blocks.unpowered_comparator || TargetBlock == Blocks.powered_comparator) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, (Meta / 4) * 4 + (((Meta % 4) + 1) % 4), 3);
                return true;
            }
            if (TargetBlock == Blocks.pumpkin || TargetBlock == Blocks.lit_pumpkin || TargetBlock == Blocks.furnace || TargetBlock == Blocks.lit_furnace || TargetBlock == Blocks.chest || TargetBlock == Blocks.trapped_chest || TargetBlock == Blocks.ender_chest) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, ((Meta - 1) % 4) + 2, 3);
                return true;
            }
            if (TargetBlock == Blocks.hopper) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, (Meta + 1) % 6 <= 6 ? (Meta + 1) % 6 : 0, 3);
                return true;
            }
            if (TargetBlock == Blocks.quartz_block && (Meta <=4 && Meta >= 2)) {
                par3World.setBlockMetadataWithNotify(X, Y, Z, Meta == 2 || Meta == 3 ? (Meta + 1) : 2, 3);
                return true;
            }
            if (TargetBlock == Blocks.lit_redstone_lamp) {
                par3World.isRemote = true;
                par3World.setBlock(X, Y, Z, Blocks.redstone_lamp, 0, 0);
                par3World.isRemote = false;
                return true;
            }
            if (TargetBlock == Blocks.redstone_lamp) {
                par3World.isRemote = true;
                par3World.setBlock(X, Y, Z, Blocks.lit_redstone_lamp, 0, 0);
                par3World.isRemote = false;
                return true;
            }
            if (TargetBlock == Blocks.golden_rail) {
                par3World.isRemote = true;
                par3World.setBlock(X, Y, Z, TargetBlock, (Meta + 8) % 16, 0);
                par3World.isRemote = false;
                return true;
            }
            if (TargetBlock == Blocks.activator_rail) {
                par3World.isRemote = true;
                par3World.setBlock(X, Y, Z, TargetBlock, (Meta + 8) % 16, 0);
                par3World.isRemote = false;
                return true;
            }
            if (aTileEntity != null && aTileEntity instanceof ic2.api.tile.IWrenchable) {
                if (((ic2.api.tile.IWrenchable)aTileEntity).wrenchCanSetFacing(par2EntityPlayer, par7)) {
                    ((ic2.api.tile.IWrenchable)aTileEntity).setFacing((short) par7);
                }
                return true;
            }
            if (TargetBlock != null && TargetBlock instanceof IWrenchable) {
                boolean result = ((IWrenchable) par3World.getBlock(X, Y, Z)).onWrench(par1ItemStack, par2EntityPlayer, par3World, X, Y, Z, par7, par8, par9, par10);
                if (par3World.getTileEntity(X, Y, Z) != null && par3World.getTileEntity(X, Y, Z) instanceof IWrenchable) {
                    ((IWrenchable) par3World.getTileEntity(X, Y, Z)).onWrench(par1ItemStack, par2EntityPlayer, par3World, X, Y, Z, par7, par8, par9, par10);

                }
                return result;
            }/*
            else if (aTileEntity instanceof ShaftMachine) {
                ShaftMachine sm = (ShaftMachine)aTileEntity;
                sm.setIORenderAlpha(512);
                par3World.markBlockForUpdate(X, Y, Z);
            }
            else if (aTileEntity instanceof Screwdriverable) {
                Screwdriverable sc = (Screwdriverable)aTileEntity;
                boolean flag = false;
                flag = sc.onRightClick(par3World, X, Y, Z, ForgeDirection.VALID_DIRECTIONS[par7]);
                return true;
            }*/
            else
            {
                if (Arrays.asList(TargetBlock.getValidRotations(par3World, X, Y, Z)).contains(ForgeDirection.getOrientation(par7))) {
                    if (TargetBlock.rotateBlock(par3World, X, Y, Z, side))
                    {
                        TargetBlock.onNeighborBlockChange(par3World, X, Y, Z, Blocks.air);
                        return true;
                    }
                }
            }
        }
        else if (par2EntityPlayer.isSneaking()){
            try {
                PlayerInteractEvent event = new PlayerInteractEvent(par2EntityPlayer, PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK, X, Y, Z, par7, par3World);
                if (MinecraftForge.EVENT_BUS.post(event) || event.getResult() == Event.Result.DENY || event.useBlock == Event.Result.DENY || event.useItem == Event.Result.DENY) {
                    return false;}
                if (ServerHelper.isServerWorld(par3World) && TargetBlock instanceof IDismantleable
                        && ((IDismantleable) TargetBlock).canDismantle(par2EntityPlayer, par3World, X, Y, Z)) {
                    ((IDismantleable) TargetBlock).dismantleBlock(par2EntityPlayer, par3World, X, Y, Z, false);
                    return true;
                }
                if (par3World.isRemote == false && (TargetBlock == Blocks.crafting_table || TargetBlock == Blocks.bookshelf || TargetBlock == Blocks.furnace || TargetBlock == Blocks.lit_furnace || TargetBlock == Blocks.chest || TargetBlock == Blocks.trapped_chest || TargetBlock == Blocks.ender_chest || TargetBlock == Blocks.piston || TargetBlock == Blocks.sticky_piston || TargetBlock == Blocks.dispenser || TargetBlock == Blocks.dropper)) {
                    EntityItem blockDropped = new EntityItem(par3World, X+0.5, Y+0.5, Z+0.5, is);
                    par3World.setBlockToAir(X, Y, Z);
                    par3World.spawnEntityInWorld(blockDropped);
                    return true;
                }
                if (TargetBlock == Blocks.rail) {
                    par3World.isRemote = true;
                    par3World.setBlock(X, Y, Z, TargetBlock, (Meta + 1) % 10, 0);
                    par3World.isRemote = false;
                    return true;
                }
                if ((TargetBlock == Blocks.detector_rail || TargetBlock == Blocks.activator_rail || TargetBlock == Blocks.golden_rail)) {
                    par3World.isRemote = true;
                    par3World.setBlock(X, Y, Z, TargetBlock, ((Meta / 8) * 8) + (((Meta % 8) + 1) % 6), 0);
                    par3World.isRemote = false;
                    return true;
                }
                /*else if (aTileEntity instanceof Screwdriverable) {
                    Screwdriverable sc = (Screwdriverable)aTileEntity;
                    boolean flag = false;
                    flag = sc.onShiftRightClick(par3World, X, Y, Z, ForgeDirection.VALID_DIRECTIONS[par7]);
                    return true;
                }*/
                if (((ic2.api.tile.IWrenchable)aTileEntity).wrenchCanRemove(par2EntityPlayer)) {
                    ItemStack tOutput = ((ic2.api.tile.IWrenchable) aTileEntity).getWrenchDrop(par2EntityPlayer);
                    for (ItemStack tStack : TargetBlock.getDrops(par3World, X, Y, Z, Meta, 0)) {
                        if (tOutput == null) {
                            par3World.spawnEntityInWorld(new EntityItem(par3World, X + 0.5, Y + 0.5, Z + 0.5, tStack));
                        } else {
                            par3World.spawnEntityInWorld(new EntityItem(par3World, X + 0.5, Y + 0.5, Z + 0.5, tOutput));
                            tOutput = null;
                        }
                        par3World.setBlockToAir(X, Y, Z);
                    }
                    return true;
                }
            } catch(Throwable e) {}
        }
        return false;

    }

    @SideOnly(Side.CLIENT)
    public boolean isFull3D()
    {
        return true;
    }

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int X, int Y, int Z, int par7, float par8, float par9, float par10)
    {
        TileEntity te = par3World.getTileEntity(X, Y, Z);
        if (te instanceof ShaftMachine) {
            ShaftMachine sm = (ShaftMachine)te;
            sm.setIORenderAlpha(512);
            par3World.markBlockForUpdate(X, Y, Z);
        }
        if (te instanceof Screwdriverable) {
            Screwdriverable sc = (Screwdriverable)te;
            boolean flag = false;
            if (par2EntityPlayer.isSneaking())
                flag = sc.onShiftRightClick(par3World, X, Y, Z, ForgeDirection.VALID_DIRECTIONS[par7]);
            else
                flag = sc.onRightClick(par3World, X, Y, Z, ForgeDirection.VALID_DIRECTIONS[par7]);
            if (flag)
                return true;
        }
        return false;
    }

    public boolean doesContainerItemLeaveCraftingGrid(ItemStack p_77630_1_)
    {
        return false;
    }
    @Override
    public boolean hasContainerItem(){
        return true;
    }
    public ItemStack getContainerItem(ItemStack is){
        return is;
    }

    //Loadsa implemented apis

    //BuildCraft
    @Override
    public boolean canWrench(EntityPlayer entityPlayer, int i, int i2, int i3) {
        return true;
    }

    @Override
    public void wrenchUsed(EntityPlayer entityPlayer, int i, int i2, int i3) {

    }

    //Applied Energistics 2
    @Override
    public boolean canWrench(ItemStack itemStack, EntityPlayer entityPlayer, int i, int i2, int i3) {
        return true;
    }

    //RailCraft
    @Override
    public boolean canWhack(EntityPlayer player, ItemStack crowbar, int x, int y, int z) {
        if (!player.isSneaking()) return true; else return false;
    }

    @Override
    public void onWhack(EntityPlayer player, ItemStack crowbar, int x, int y, int z) {
        player.swingItem();
    }

    @Override
    public boolean canLink(EntityPlayer player, ItemStack crowbar, EntityMinecart cart) {
        if (player.isSneaking()) return true; else return false;
    }

    @Override
    public void onLink(EntityPlayer player, ItemStack crowbar, EntityMinecart cart) {
        player.swingItem();
    }

    @Override
    public boolean canBoost(EntityPlayer player, ItemStack crowbar, EntityMinecart cart) {
        if (!player.isSneaking()) return true; else return false;
    }

    @Override
    public void onBoost(EntityPlayer player, ItemStack crowbar, EntityMinecart cart) {
        player.swingItem();
    }

    //Carpenter's Blocks
    @Override
    public void onHammerUse(World world, EntityPlayer player) {
        player.swingItem();
    }

    @Override
    public boolean canUseHammer(World world, EntityPlayer player) {
        return true;
    }

    //Project:Red
    @Override
    public void damageScrewdriver(World world, EntityPlayer player) {
        player.swingItem();
    }

    //I dunno which mod uses this, but CoFH API contains this
    @Override
    public boolean isUsable(ItemStack item, EntityLivingBase user, int x, int y, int z) {
        return true;
    }

    @Override
    public void toolUsed(ItemStack item, EntityLivingBase user, int x, int y, int z) {
        user.swingItem();
    }

    //MineFactory Reloaded
    @Override
    public boolean isActive(ItemStack stack) {
        return true;
    }

    //Hello from future of Mekanism
    @Override
    public boolean canUseWrench(EntityPlayer player, int x, int y, int z) {
        return true;
    }

}