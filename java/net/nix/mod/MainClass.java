package net.nix.mod;

import com.asaskevich.smartcursor.api.ModuleConnector;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

@Mod(modid = MainClass.MODID, version = MainClass.VERSION)
public class MainClass {

    public static final String MODID = "screwdriver";
    public static final String VERSION = "2.0";


    @SidedProxy(clientSide = "net.nix.mod.ClientProxy", serverSide = "net.nix.mod.CommonProxy")
    public static CommonProxy proxy;

    public static Item Screwdriver;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {

        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        config.save();

        if (Loader.isModLoaded("gregtech_addon")) {
            System.err.println("[Screwdriver] Screwdriver doesn't work with GregTech machinery. You was warned.");
        }
        Screwdriver = new Screwdriver().setUnlocalizedName("toolScrewdriver").setTextureName("hlestamod:toolScrewdriver");
        GameRegistry.registerItem(Screwdriver, Screwdriver.getUnlocalizedName());

        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(Screwdriver), new Object[]{"IXX", "XIW", "XWS", 'I', "ingotIron", 'W', new ItemStack(Blocks.stained_hardened_clay, 1, 14), 'S', "stickWood"}));
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(Screwdriver), new Object[]{"IXX", "XIW", "XWS", 'I', "rodIron", 'W', new ItemStack(Blocks.stained_hardened_clay, 1, 14), 'S', "stickWood"}));

        ItemStack book = new ItemStack(Items.written_book);
        NBTTagList bookPages = new NBTTagList();
        bookPages.appendTag(new NBTTagString("Hello, fellow Screwdriver user! This book will tell you about Screwdriver interaction with your installed mods!"));
        if (Loader.isModLoaded("BuildCraft|Core")) {
            bookPages.appendTag(new NBTTagString("It looks like you have BuildCraft! You can use screwdriver as BuildCraft Wrench!"));
        }
        if (Loader.isModLoaded("IC2")) {
            bookPages.appendTag(new NBTTagString("It looks like you have IndustrialCraft 2! So, yes, you can wrench your machines with screwdriver!"));
        }
        if (Loader.isModLoaded("ThermalExpansion")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Thermal Expansion! Screwdriver has full compatibility with it!"));
        }
        if (Loader.isModLoaded("appliedenergistics2")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Applied Energistics 2! You can use screwdriver as AE2 Wrench!"));
        }
        if (Loader.isModLoaded("Railcraft")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Railcraft! Yes, you can link carts, rotate rails just like Crowbar!"));
        }
        if (Loader.isModLoaded("EnderIO")) {
            bookPages.appendTag(new NBTTagString("It looks like you have EnderIO! You can use screwdriver as Yeta Wrench!"));
        }
        if (Loader.isModLoaded("CarpentersBlocks")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Carpenters Blocks! You can use screwdriver as CB's Hammer, but not as chisel! Not to be confused!"));
        }
        if (Loader.isModLoaded("MinefactoryReloaded")) {
            bookPages.appendTag(new NBTTagString("It looks like you have MineFactory Reloaded! Screwdriver is untested with it! Tell us about MFR compatibility in da Screwdriver thread near you!"));
        }
        if (Loader.isModLoaded("Steamcraft")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Professor Flaxbeard's Wondrous Steam Power Mod! Compatibility with this mod is very buggy, I need to contact Santa to tell about it!"));
        }
        if (Loader.isModLoaded("ProjRed|Core")) {
            bookPages.appendTag(new NBTTagString("It looks like you have Project:Red! Our Screwdriver has functions of PR Screwdriver!"));
        }
        bookPages.appendTag(new NBTTagString("That's all, folks!"));
        book.setTagInfo("pages", bookPages);
        book.setTagInfo("author", new NBTTagString("nixhlestakov"));
        book.setTagInfo("title", new NBTTagString("Screwdriver Manual"));
        GameRegistry.addRecipe(new ShapelessOreRecipe(book, new Object[]{new ItemStack(Items.book, 1), new ItemStack(MainClass.Screwdriver, 1)}));
        GameRegistry.addRecipe(new ShapelessOreRecipe(book, new Object[]{new ItemStack(Items.written_book, 1), new ItemStack(MainClass.Screwdriver, 1)}));
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event){
        ModuleConnector.connectModule(new SmartCursorModule());
    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent event) {
        if (Loader.isModLoaded("Steamcraft")) {MinecraftForge.EVENT_BUS.register(new SteamcraftIntegrationHandler());}
    }
}
