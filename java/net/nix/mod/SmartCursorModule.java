package net.nix.mod;

import java.util.List;

import cofh.api.block.IDismantleable;
import com.asaskevich.smartcursor.api.IBlockProcessor;
import flaxbeard.steamcraft.api.IWrenchable;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public class SmartCursorModule implements IBlockProcessor {
    @Override
    public String getModuleName() {
        return "Magic Screwdriver Module";
    }
    @Override
    public String getAuthor() {
        return "nixhlestakov";
    }
    @Override
    public void process(List<String> list, Block block, int meta, World w, int x, int y, int z) {
        Minecraft mc = Minecraft.getMinecraft();
        MovingObjectPosition pos = mc.objectMouseOver;
        TileEntity aTileEntity = w.getTileEntity(x, y, z);
        if (pos != null && mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof Screwdriver) {
            Block TargetBlock = w.getBlock(x, y, z);
            byte Meta = (byte) w.getBlockMetadata(x, y, z);
            final ItemStack is = new ItemStack(TargetBlock, 1, Meta);
            if (OreDictionary.getOreID(is) == OreDictionary.getOreID("logWood") || TargetBlock == Blocks.hay_block || TargetBlock == Blocks.piston || TargetBlock == Blocks.sticky_piston || TargetBlock == Blocks.dispenser || TargetBlock == Blocks.dropper || TargetBlock == Blocks.unpowered_repeater || TargetBlock == Blocks.powered_repeater || TargetBlock == Blocks.pumpkin || TargetBlock == Blocks.lit_pumpkin || TargetBlock == Blocks.furnace || TargetBlock == Blocks.lit_furnace || TargetBlock == Blocks.chest || TargetBlock == Blocks.trapped_chest || TargetBlock == Blocks.ender_chest || TargetBlock == Blocks.lit_redstone_lamp || TargetBlock == Blocks.redstone_lamp || TargetBlock == Blocks.golden_rail || TargetBlock == Blocks.activator_rail || TargetBlock == Blocks.crafting_table || TargetBlock == Blocks.bookshelf || TargetBlock == Blocks.rail || TargetBlock == Blocks.detector_rail || TargetBlock instanceof IDismantleable || TargetBlock instanceof IWrenchable || aTileEntity instanceof ic2.api.tile.IWrenchable)
                list.add("Screwdriverable");
        }
    }
}